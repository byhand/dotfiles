#! /bin/zsh

source ./util.sh
export dotfiles_dir=$(pwd)

title Requesting sudo
sudo -v

./scripts/osx/install.sh
./scripts/generic/setup.sh
./scripts/osx/setup.sh
./scripts/generic/fish_install.sh
