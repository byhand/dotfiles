source ./util.sh

title setting up iterm

plist=com.googlecode.iterm2.plist

customFolder=$dotfiles_dir/apps/iterm
prefsKey=PrefsCustomFolder
currentFolder=$(defaults read $plist $prefsKey 2>/dev/null)
if [[ "$customFolder" = "$currentFolder" ]]; then
    log prefs folder already set
else
    defaults write $plist $prefsKey -string "$customFolder"
    log prefs folder set
fi

loadPrefsKey=LoadPrefsFromCustomFolder
currentLoadPrefs=$(defaults read $plist $loadPrefsKey 2>/dev/null)
if [[ "$currentLoadPrefs" = "1" ]]; then
    log prefs load already set
else
    defaults write $plist $loadPrefsKey -bool true
    log prefs load set
fi