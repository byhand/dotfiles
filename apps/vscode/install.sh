source ./util.sh

title setting up vscode

if [ "$(uname)" == "Darwin" ]; then
    code_preferences="$HOME/Library/Application Support/Code"
else
    code_preferences="$HOME/.config/Code"
fi

mkdir -p "$code_preferences/User"
settings_file="$code_preferences/User/settings.json"

link settings $dotfiles_dir/apps/vscode/settings.json "$settings_file"

log installing extensions

installed=$(code --list-extensions)
extensions=(
    adamisrael.author-stats
    eamodio.gitlens
    esbenp.prettier-vscode
    GraphQL.vscode-graphql
    hashicorp.terraform
    netcorext.uuid-generator
    silvenon.mdx
    skyapps.fish-vscode
    sleistner.vscode-fileutils
    Tyriar.sort-lines
    uloco.theme-bluloco-dark
    vscode-icons-team.vscode-icons
    wmaurer.change-case
)

for extension in ${extensions[@]}; do 
    if [[ "$installed" == *"$extensions"* ]]; then
        log $extension already installed
    else
        code --install-extension $extension
    fi
done
