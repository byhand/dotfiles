source ./util.sh

title setting up 1password

existing=$(cat $HOME/.config/op/config 2>/dev/null | grep "\"shorthand\": \"mj\"")
if [[ -z $existing ]]; then
    eval $(op signin my.1password.com maddi@maddijoyce.com --shorthand mj)
    log 1password setup complete
else
    log 1password already setup
fi
