source ./util.sh

title setting up viscosity

dotfiles_visc=$dotfiles_dir/configs/$USER_TYPE/viscosity
plist=com.viscosityvpn.Viscosity.plist

if [[ -d $dotfiles_visc ]]; then
  settings_file="$HOME/Library/Preferences/$plist"
  link plist "$dotfiles_visc/$plist" "$settings_file"

  support_folder="$HOME/Library/Application Support/Viscosity"
  mkdir -p "$support_folder"
  link vpn "$dotfiles_visc/OpenVPN" "$support_folder/OpenVPN"
fi