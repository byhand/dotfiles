#! /bin/bash

source ./util.sh
export dotfiles_dir=$(pwd)

title Requesting sudo
sudo -v

./scripts/arch/install.sh
./scripts/arch/setup.sh
./scripts/generic/setup.sh
./scripts/generic/fish_install.sh