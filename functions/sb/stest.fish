set session "stest"
set types "s tyw"
set lint "s lw"
set tests "s tew"

function stest -d "Smart Testing Watcher"
	tmux new-session -d -s $session $types
  tmux splitw -v $lint

  tmux splitw -f -h $tests

  tmux bind-key -n C-k 'kill-session'
  tmux bind-key -n C-r 'respawn-pane -k'

  tmux -2 attach -d -t $session
end
