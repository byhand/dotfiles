set label "SmartForms DotEnv"
set from "$HOME/.config/SmartForms"
set to "$HOME/Developer/smart/dev/tools/runner-local/.env"

function senv -d "Smart Env Link"
  if test -L "$to"
    echo $label already linked
  else if test ! -e "$from"
    echo $label not available at $from
  else
    if test -e "$to"
      mv "$to" "$to.bak"
    end
    ln -s "$from" "$to"
    echo linked $label
  end
end
