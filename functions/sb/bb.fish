function bb -d "Bitbucket Clone"
  if set -q argv[2]
    set project $argv[1]
    set repo $argv[2]
  else
    set project smokeballdev
    set repo $argv[1]
  end

	git clone git@bitbucket.org:$project/$repo
end
