set connection "Smokeball AU"
set item "Smokeball VPN"

function vpn -d "Smokeball VPN Connection"
  set operation $argv[1]


  switch $operation
  case up
    set loginTest (op get account --account mj 2>&1)
    if string match -q -- "*[ERROR]*" $loginTest
      eval (op signin mj)
    end
    
    set password (op get item $item --fields password)
    nmcli connection modify $connection vpn.secrets "password=$password"
    
    set mask (string replace -ar "." "\b" $password)
    set token (op get totp $item)

    printf "$mask$token" | nmcli connection up $connection --ask
  case down
    nmcli connection down $connection
  case '*'
    echo Unknown operation
	end
end
