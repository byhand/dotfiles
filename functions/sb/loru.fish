function loru -d "Loop Runner"
  set -q argv[1]; and set rtype $argv[1]; or set rtype all
  LOOP_RUNNER_CONFIG=$COMMUNICATE_LOCALDB_LOCATION/runner-$rtype.json loop-runner
end
