function db-creds -d "Communicate DB Credentials"
	set region $argv[1]
  set appenv $argv[2]
  set -q argv[3]; and set userType $argv[3]; or set userType ReadOnly

  switch $appenv
  case dev staging
    set dataenv com-datastaging
  case qa live
    set dataenv com-datalive
  end

  if set -q region; and set -q appenv; and set -q dataenv
    set -x AWS_PROFILE "com$region$appenv"
    set params (aws ssm get-parameters-by-path --path "/communicate/$region/$dataenv/common")
  
    set userName comdb{$userType}User
    set user (echo $params | jq -r --arg param $userName ".Parameters[] | select(.Name | contains(\$param)) | .Value" | jq -r)
    set config (echo $params | jq -r '.Parameters[] | select(.Name | contains("comdbKnexConfig")) | .Value')
    set host (echo $config | jq -r '.connection.host')
    set db (echo $config | jq -r '.connection.database')

    set secretPath /communicate/$region/$dataenv/secret/comdb{$userType}Password
    set secret (aws ssm get-parameters --names "$secretPath" --with-decryption)
    set pass (echo $secret | jq -r '.Parameters[0].Value' | jq -r)

    echo "mysql://$user:$pass@$host:3306/$db"
  else 
    echo "Region and app env are required, e.g. db-creds aus dev"
  end
end
