set session "sdev"
set proxy "s d dev-proxy-local"
set types "s tyw"

set panels "s d dev-runner-local"
set pcount (count $panels)

function sdev -d "Smart Dev Environment"
  tmux new -d -s $session "$proxy"
  tmux splitw -p 70 -v $types

  if test $pcount -gt 0
    tmux splitw -f -h $panels[1]
  end
  tmux bind-key -n C-k 'kill-session'
  tmux bind-key -n C-r 'respawn-pane -k'

  if test $pcount -gt 1
    set swapper "swap-pane -s 0.2 -t 1.0"

    set index 1
    for panel in $panels[2..-1]
      tmux neww -d -t $session $panel
      
      set next (math $index + 1)
      if test $next -lt $pcount
        set swapper "$swapper; swap-pane -s $index.0 -t $next.0"
      end
      set index $next
    end

    tmux bind-key -n C-s "$swapper"
  end

  tmux -2 attach -d -t $session
end
