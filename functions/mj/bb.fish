function bb -d "Bitbucket Clone"
  set project $argv[1]
  set repo $argv[2]

  switch $project
  case bh
    set project byhand
  case jl
    set project joylevel
  end

	git clone git@bitbucket.org:$project/$repo
end
