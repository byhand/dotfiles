function 1pdocsop 
  set operation $argv[1]
  set vault $argv[2]
  set title $argv[3]
  set filePath $argv[4]

  echo $operation $title to $filePath

  switch $operation
  case load
    if test -e "$filePath"
      rm "$filePath"
    end

    echo downloading $filePath
    op get document $title --output "$filePath"
    chmod 600 "$filePath"
    echo $filePath downloaded
  case save
    if test -e "$filePath"
      op delete document $title
      echo uploading $filePath
      op create document "$filePath" --vault $vault --title $title
      echo $filePath uploaded
    else
      echo $filePath does not exist
    end
  case '*'
    echo Unknown operation
  end
end

function 1pdocs
  set loginTest (op get account --account mj 2>&1)
  if string match -q -- "*[ERROR]*" $loginTest
    eval (op signin mj)
  end

  switch $USER_TYPE
  case mj
    set vault Personal
  case sb
    set vault Smokeball
  end

  set operation $argv[1]
  while read -la line
    set parts (string split = "$line")
    set title $parts[1]
    set loc (eval echo $parts[2])

    1pdocsop $operation $vault "$title - $vault" "$loc"
  end < $HOME/.config/fish/secrets.txt
end