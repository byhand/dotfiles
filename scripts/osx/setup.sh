source ./util.sh

title Setting up Apps
osx_apps=(
    1password
    iterm
    vscode
    viscosity
)
for app in ${osx_apps[@]}; do
    $dotfiles_dir/apps/$app/install.sh
done