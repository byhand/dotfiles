source ./util.sh

title Installing Keyboard Layout
for bundle in $dotfiles_dir/configs/osx/layouts/*; do
    bundle_name=$(basename "$bundle")
    to="/Library/Keyboard Layouts/$bundle_name"
    if [[ -d $to ]]; then
        log $bundle_name already installed
    else
        sudo cp -r "$bundle" "/Library/Keyboard Layouts/$bundle_name"
        log copied $bundle_name
    fi
done

brew_loc=$(which brew)
title Installing Homebrew
if [[ -z $brew_loc ]]; then
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   eval "$(/opt/homebrew/bin/brew shellenv)"
else
    log brew already installed
fi

brew_installed=$(brew list)
brew_install_if_needed() {
    is_installed=$(echo $brew_installed | grep -o "\b$1\b")
    if [[ -z $is_installed ]]; then
        brew install $1
        log installed $1
    else
        log $1 already installed
    fi
}

title Installing CLI Utils
cli_utilities=(
    fish
    go
    mas
    starship
    awscli
    1password-cli
    jq
)
for util in ${cli_utilities[@]}; do
    brew_install_if_needed $util
done

title Installing Fonts
brew tap homebrew/cask-fonts
fonts=(
    fira-code
    fira-code-nerd-font
)
for font in ${fonts[@]}; do
    brew_install_if_needed font-$font
done

title Installing Apps
apps=(
    1password
    amethyst
    firefox
    google-chrome
    iterm2
    rectangle
    slack
    visual-studio-code
    zoom
    tableplus
    viscosity
)
for app in ${apps[@]}; do
    brew_install_if_needed $app
done

mas_installed=$(mas list)
mas_install_if_needed() {
    is_installed=$(echo $mas_installed | grep -o "\b$1\b")
    if [[ -z $is_installed ]]; then
        mas install $1
        log installed $1
    else
        log $1 already installed
    fi
}

title Installing App Store Apps
store_apps=(
    1489591003 # Edison
    404705039  # Graphic
    1289583905 # Pixelmator Pro
#    497799835  # Xcode
)
for store_app in ${store_apps[@]}; do
    mas_install_if_needed $store_app
done

brew_path=$(brew --prefix)/bin
has_brew=$(fish -c "echo \$fish_user_paths" | grep -o $brew_path)
if [[ -z $has_brew ]]; then
    fish -c "set -Ua fish_user_paths $brew_path"    
    log added brew to path
else
    log brew already available
fi
