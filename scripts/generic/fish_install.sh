#!/usr/bin/env fish

function title
    echo "# $argv..."
end
function log
    echo "    $argv..."
end

title Installing fisher
set has_fisher (fisher)
if test -z "$has_fisher"
    curl -sL https://git.io/fisher | source && fisher install jorgebucaran/fisher
    log installed fisher
else
    log fisher already available
end

set fisher_installed (fisher list)
function fisher_install_if_needed
    set is_installed (echo $fisher_installed | grep -o "\b$argv\b")
    if test -z $is_installed
        fisher install $argv
        log installed $argv
    else
        log $argv already installed
    end
end

set fishers jorgebucaran/nvm.fish jethrokuan/z
for fisher in $fishers
    fisher_install_if_needed $fisher
end
