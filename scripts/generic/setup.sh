source ./util.sh

title Setting shell to fish
fish_path=$(which fish)
log fish is here - $fish_path

has_fish=$(sudo cat /etc/shells | grep fish)
if [[ -z $has_fish ]]; then
    echo $fish_path | sudo tee -a /etc/shells
else
    log fish already allowed
fi

if [[ $fish_path == $SHELL ]]; then
    log fish already default
else
    chsh -s $fish_path
fi

title Setting up config
fish_config=$HOME/.config/fish
if [[ -n $USER_TYPE ]]; then
    log Using $USER_TYPE configs
    mkdir -p $fish_config

    link "fish config" $dotfiles_dir/configs/$USER_TYPE/config.fish $fish_config/config.fish
    link "secrets" $dotfiles_dir/configs/$USER_TYPE/secrets.txt $fish_config/secrets.txt
    link "starship config" $dotfiles_dir/configs/$USER_TYPE/starship.toml $HOME/.config/starship.toml

    link "ssh files" $dotfiles_dir/configs/$USER_TYPE/ssh $HOME/.ssh
    link "aws files" $dotfiles_dir/configs/$USER_TYPE/aws $HOME/.aws
    link "git files" $dotfiles_dir/configs/$USER_TYPE/git $HOME/.config/git
    link "tmux files" $dotfiles_dir/configs/$USER_TYPE/tmux $HOME/.config/tmux
fi

title Setting up functions
fish_functions=$fish_config/functions
mkdir -p $fish_functions

log Linking common functions
for function in $dotfiles_dir/functions/common/*; do
    function_name=$(basename "$function")
    rm $fish_functions/$function_name
    ln -s $function $fish_functions/$function_name
done
if [[ -n $USER_TYPE ]]; then
    log Linking $USER_TYPE functions
    
    if [[ -d $dotfiles_dir/functions/$USER_TYPE ]]; then
        for function in $dotfiles_dir/functions/$USER_TYPE/*; do
            function_name=$(basename "$function")
            rm $fish_functions/$function_name
            ln -s $function $fish_functions/$function_name
        done
    fi
fi