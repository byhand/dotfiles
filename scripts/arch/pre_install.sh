#! /bin/bash

loadkeys dvorak-programmer

# Use lsblk to determine disk location
disk_loc=/dev/sda

cfdisk $disk_loc
# Add 512m EFI System
# Add Rest Linux filesystem

mkfs.ext4 ${disk_loc}2
mkfs.fat -F32 ${disk_loc}1

mount ${disk_loc}2 /mnt
mkdir -p /mnt/boot/efi
mount ${disk_loc}1 /mnt/boot/efi

cp /etc/pacman.d/mirrorlist /etc/pacman.d/mirrorlist.bak
reflector -c "AU" -f 12 -l 10 -n 12 --save /etc/pacman.d/mirrorlist

pacstrap /mnt base base-devel linux linux-firmware vim

genfstab -U /mnt >> /mnt/etc/fstab

arch-chroot /mnt

timedatectl set-timezone Australia/Adelaide
timedatectl set-ntp true

sed -i 's/^#en_AU.UTF-8/en_AU.UTF-8/' /etc/locale.gen
locale-gen
export LANG=en_AU.UTF-8
echo LANG=$LANG > /etc/locale.conf

localectl set-keymap --no-convert dvorak-programmer

echo host_name > /etc/hostname

pacman -S dhcpcd qemu-guest-agent openssh
systemctl enable dhcpcd qemu-guest-agent sshd

passwd
# Set root password

pacman -S grub efibootmgr
grub-install --target=x86_64-efi --bootloader-id=arch_grub --efi-directory=/boot/efi
grub-mkconfig -o /boot/grub/grub.cfg

groupadd -r autologin
useradd -m -g users -G wheel,storage,power,autologin,audio maddi
passwd maddi

EDITOR=vim visudo
# remove comment on line that includes %wheel NOPASSWD

systemctl edit getty@tty1
# [Service]
# ExecStart=
# ExecStart=-/usr/bin/agetty --autologin maddi --noclear %I $TERM

pacman -S \
  sway alacritty waybar wofi \
  xorg-xwayland xorg-xlsclients qt5-wayland glf-wayland \
  pulseaudio

exit
umount -R /mnt
poweroff

