source ./util.sh

pacman_install_if_needed() {
    is_installed=$(pacman -Qi $1 2>&1 | grep "Install Date")
    if [[ -z $is_installed ]]; then
        sudo pacman -Sy --noconfirm $1
        log installed $1
    else
        log $1 already installed
    fi
}

aur_install_if_needed() {
    base_dir=$HOME/.packages
    aur_dir=$base_dir/$1
    is_installed=$(pacman -Qi $1 2>&1 | grep "Install Date")
    if [[ -z $is_installed ]]; then
        mkdir -p $base_dir
        git clone https://aur.archlinux.org/$1.git $aur_dir
        cd $aur_dir
        makepkg -sri --noconfirm
        log installed $1
    else
	    log $1 already installed
    fi
}

title Installing Programs
progs=(
    fish
    go
    starship
    aws-cli
    code
    firefox
)
for prog in ${progs[@]}; do
    pacman_install_if_needed $prog
done
aurs=(
    1password-cli
    visual-studio-code-bin
    clipman
)
for aur in ${aurs[@]}; do
    aur_install_if_needed $aur
done

title Installing Fonts
fonts=(
    nerd-fonts-fira-code
    otf-apple-sf-pro
)
for font in ${fonts[@]}; do
    aur_install_if_needed $font
done
