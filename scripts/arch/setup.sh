source ./util.sh

link "sway config" $dotfiles_dir/configs/arch/sway $HOME/.config/sway
link "alacritty config" $dotfiles_dir/configs/arch/alacritty $HOME/.config/alacritty
link "wofi config" $dotfiles_dir/configs/arch/wofi $HOME/.config/wofi
link "waybar config" $dotfiles_dir/configs/arch/waybar $HOME/.config/waybar
link "tmux config" $dotfiles_dir/configs/arch/tmux $HOME/.config/tmux

title Setting up Apps
arch_apps=(
    1password
    vscode
)
for app in ${arch_apps[@]}; do
    $dotfiles_dir/apps/$app/install.sh
done
