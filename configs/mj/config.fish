starship init fish | source

export USER_TYPE=mj

alias gclean "git branch --no-color --merged | egrep -v \"(^\*|master)\" | xargs git branch -d"

set -x MOZ_ENABLE_WAYLAND 1
