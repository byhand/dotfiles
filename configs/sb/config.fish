starship init fish | source

export USER_TYPE=sb

alias gclean "git branch --no-color --merged | egrep -v \"(^\*|main)\" | xargs git branch -d"

export NODE_OPTIONS="--max-old-space-size=16000"
export COMMUNICATE_DEVELOPER_FLAG=MJ
export COMMUNICATE_LOCALDB_LOCATION=$HOME/Developer/smokeball-localdb
export SMART_HOME=$HOME/Developer/smart-localdb

source $HOME/.config/fish/functions/__dir_change.fish

export ANDROID_HOME=$HOME/Library/Android/sdk

set -x MOZ_ENABLE_WAYLAND 1
