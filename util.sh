title() {
    echo "# $@..."
}
log() {
    echo "    $@..."
}
link() {
    label=$1
    from=$2
    to=$3

    if [[ -L "$to" ]]; then
        log $label already linked
    else
        if [[ -e "$to" ]]; then
            mv "$to" "$to.bak"
        fi
        ln -s "$from" "$to"
        log linked $label
    fi
}